# Unity Spiral Mesh

Read the article [Procedural Spiral Mesh in Unity](https://derekknox.com/articles/procedural-spiral-mesh-in-unity/) for details.

![Spiral Mesh Example](https://derekknox.com/articles/procedural-spiral-mesh-in-unity/assets/img/spiral-with-material.png?v1)